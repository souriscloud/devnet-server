const server = require('./app/lib/express.lib');

const apiRouterV1 = require('./app/router/v1.api.router'),
  userRouterV1 = require('./app/router/v1/user.api.router'),
  clientRouterV1 = require('./app/router/v1/client.api.router');

const tokenMiddleware = require('./app/middleware/token.middleware');

const middlewares = [
  {
    path: '/api',
    middleware: tokenMiddleware
  }
];

const routers = [
  {
    path: '/api/v1',
    router: apiRouterV1()
  },{
    path: '/api/v1/user',
    router: userRouterV1()
  },{
    path: '/api/v1/client',
    router: clientRouterV1()
  }
];

const app = server(routers, middlewares);