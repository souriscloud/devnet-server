var mongoose = require('../lib/mongoose.lib')(),
  Schema = mongoose.Schema;

const UserSchema = new Schema({
  userName: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  firstName: String,
  lastName: String,
  registeredOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('User', UserSchema);