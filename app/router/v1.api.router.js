const expressRouter = require('express').Router();

const Message = require('../model/message.model');
const User = require('../model/user.model');

module.exports = () => {
  expressRouter.get('/', (req, res) => {
    Message.find((err, messages) => {
      if (err) {
        console.error(err);
        res.sendStatus(400);
        return;
      }

      res.json(messages);
    });
  });

  expressRouter.post('/', (req, res) => {
    console.log(req.body);
    const newMessage = new Message({
      body: req.body.message
    });
    newMessage.save((err) => {
      if (err) {
        console.error(err);
        res.sendStatus(400);
        return;
      }

      res.sendStatus(200);
    });
  });

  expressRouter.post('/auth', (req, res) => {
    User.findOne({'userName': req.body.username, 
      'password': req.body.password}, "-password").exec(function (err, user) {
        if (err) {
          console.error(err);
          res.sendStatus(400);
          return;
        }

        if (user === null) {
          res.sendStatus(400);
          return;
        }

        res.json(user);
      });
  });

  return expressRouter;
};