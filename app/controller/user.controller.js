const User = require('../model/user.model');

module.exports = {
  list: (req, res) => {
    User.find((err, users) => {
      if (err) {
        console.error(err);
        res.sendStatus(400);
        return;
      }

      res.json(users);
    });
  },
  new: (req, res) => {
    const newUser = new User({
      userName: req.body.user.userName,
      password: req.body.user.password
    });
    newUser.save((err) => {
      if (err) {
        console.error(err);
        res.sendStatus(400);
        return;
      }

      res.json(newUser);
    });
  }
};